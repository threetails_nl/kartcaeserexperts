@extends('layouts.standard')

@section('content')

    <div class="graph">
        <div class="row">
            <div class="col-md-12 box">
                <canvas id="lineChart" width="200" height="200"></canvas>
            </div>
        </div>
    </div>

@endsection
