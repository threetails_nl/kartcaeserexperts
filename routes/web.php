<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/racetrack', 'RacetrackController@index');
Route::get('/sample', 'RacetrackController@sample');
Route::get('/trackmouse', 'RacetrackController@trackMouse');
Route::get('/getracerdata', 'RacetrackController@getracerdata');
Route::get('/getracerdata/{pass}', 'RacetrackController@getracerdata');

Route::get('/data', 'dataController@index');

Route::get('/getdata', 'dataController@getData');
