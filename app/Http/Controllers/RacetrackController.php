<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RacetrackController extends Controller
{
    public function index()
    {

        $data = file_get_contents("https://microsoft-apiapp7284340bfaae473db52911091c6ff304.azurewebsites.net/api/kartinfo");

//        return $data;

//        $coords = array(
//            array("kart_id" => "0xDECA3130371013CA", "kart_position" => "3", "kart_speed" => "0", "kart_time" => "102.452", "kart_x" => "-34.2704", "kart_y" => "18.7984"),
//            array("kart_id" => "0xDECA343036000F72", "kart_position" => "2", "kart_speed" => "0", "kart_time" => "43.357", "kart_x" => "-13.7441", "kart_y" => "7.55823"),
//            array("kart_id" => "0xDECA3530360008E9", "kart_position" => "1", "kart_speed" => "0", "kart_time" => "43.335", "kart_x" => "-13.5244", "kart_y" => "6.74833"),
//            array("kart_id" => "0xDECA3030354004C5", "kart_position" => "0", "kart_speed" => "0", "kart_time" => "0", "kart_x" => "10.9504", "kart_y" => "16.9633"),
//            array("kart_id" => "0xDECA3330354004B1", "kart_position" => "0", "kart_speed" => "0", "kart_time" => "0", "kart_x" => "17.062", "kart_y" => "16.7226"),
//            array("kart_id" => "0xDECA323035400489", "kart_position" => "0", "kart_speed" => "0", "kart_time" => "0", "kart_x" => "2.51987", "kart_y" => "6.76239"),
//            array("kart_id" => "0xDECA31303540049E", "kart_position" => "0", "kart_speed" => "0", "kart_time" => "0", "kart_x" => "3.84036", "kart_y" => "10.3787")
//        );
//
//       dd(json_encode($coords));

        return view('racetrack.index', compact('data'));

    }

    public function sample()
    {

        $data = file_get_contents("https://microsoft-apiapp7284340bfaae473db52911091c6ff304.azurewebsites.net/api/kartinfo");

//        return $data;

//        $coords = array(
//            array("kart_id" => "0xDECA3130371013CA", "kart_position" => "3", "kart_speed" => "0", "kart_time" => "102.452", "kart_x" => "-34.2704", "kart_y" => "18.7984"),
//            array("kart_id" => "0xDECA343036000F72", "kart_position" => "2", "kart_speed" => "0", "kart_time" => "43.357", "kart_x" => "-13.7441", "kart_y" => "7.55823"),
//            array("kart_id" => "0xDECA3530360008E9", "kart_position" => "1", "kart_speed" => "0", "kart_time" => "43.335", "kart_x" => "-13.5244", "kart_y" => "6.74833"),
//            array("kart_id" => "0xDECA3030354004C5", "kart_position" => "0", "kart_speed" => "0", "kart_time" => "0", "kart_x" => "10.9504", "kart_y" => "16.9633"),
//            array("kart_id" => "0xDECA3330354004B1", "kart_position" => "0", "kart_speed" => "0", "kart_time" => "0", "kart_x" => "17.062", "kart_y" => "16.7226"),
//            array("kart_id" => "0xDECA323035400489", "kart_position" => "0", "kart_speed" => "0", "kart_time" => "0", "kart_x" => "2.51987", "kart_y" => "6.76239"),
//            array("kart_id" => "0xDECA31303540049E", "kart_position" => "0", "kart_speed" => "0", "kart_time" => "0", "kart_x" => "3.84036", "kart_y" => "10.3787")
//        );
//
//       dd(json_encode($coords));

        return view('racetrack.sample', compact('data'));

    }

    public function trackMouse()
    {

        return view('racetrack.trackmouse');

    }

    public function getRacerData($pass)
    {
//        $data = json_decode(file_get_contents("https://microsoft-apiapp7284340bfaae473db52911091c6ff304.azurewebsites.net/api/kartinfo"));
        $data = json_decode(file_get_contents("http://www.arjenschrijer.com/kartapi/index.php?method=getKartData&pass=" . $pass . ""));
//        $countData = file_get_contents("http://www.arjenschrijer.com/kartapi/index.php?method=countKartData");

        return response()->json($data);
    }
}
