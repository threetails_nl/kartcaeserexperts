<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kart_info;

class DataController extends Controller
{
    public $kartingTime;
    public $laptimes;

    public function __construct()
    {
        $this->kartingTime = array(
            array("naam" => "Rijder 1","hoogste_tijd" => 35.44, "lap_1" => 36.25, "lap_2" => 35.87, "lap_3" => 36.79, "lap_4" => 35.49, "lap_5" => 36.23),
            array("naam" => "Rijder 2","hoogste_tijd" => 36.11, "lap_1" => 39.02, "lap_2" => 38.30, "lap_3" => 38.88, "lap_4" => 38.33, "lap_5" => 39.98)
        );

        $this->laptimes = json_encode($this->kartingTime);
    }

    public function index()
    {
        $laptimes = json_decode($this->laptimes, true);

        $kartlaps = $this->getData();




        return view('dashboard.index', compact('laptimes', 'kartlaps'));
    }


    public function getData()
    {
        $data = json_decode(file_get_contents("https://microsoft-apiapp7284340bfaae473db52911091c6ff304.azurewebsites.net/api/kartinfo"), true);

        return $data;

    }




}
