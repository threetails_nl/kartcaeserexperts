<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kart_info extends Model
{
    protected $fillable = ['kart_id','kart_position', 'kart_speed', 'kart_time', 'kart_x', 'kart_y', 'datetime'];

    protected $table = 'kart_info';

}
