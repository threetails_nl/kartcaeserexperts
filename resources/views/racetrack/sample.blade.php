<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        #track {
            visibility: hidden;
        }

        #raceTrack {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 1;
        }

        #raceTrackRoute {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 2;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <img src="/img/track.svg" id="track">
        <canvas id="raceTrack" width="1500" height="700"></canvas>
        <canvas id="raceTrackRoute" width="1500" height="700"></canvas>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script>
    window.onload = function () {
        var cTrack = document.getElementById("raceTrack");
        var ctxTrack = cTrack.getContext("2d");

        var cTrackRoute = document.getElementById("raceTrackRoute");
        var ctxTrackRoute = cTrackRoute.getContext("2d");

        var img = document.getElementById("track");
        ctxTrack.drawImage(img, 0, 0);

        var pass = 0;

        setInterval(function () {
            $.get("/getracerdata/" + (pass) + "", function (data) {
                if (pass <= 878) {
                    console.log(data.length);
                    for (var i = 0; i < data.length; i++) {
                        console.log(data);
                        console.log(data['kart_x']);

                        var x = data[i]['kart_x'];
                        var y = data[i]['kart_y'];

                        ctxTrackRoute.clearRect(0, 0, 1500, 700);

                        ctxTrackRoute.beginPath();
                        ctxTrackRoute.arc(x, y, 10, 0, 2 * Math.PI);

                        ctxTrackRoute.fillStyle = "red";
                        ctxTrackRoute.fill();
                        ctxTrackRoute.lineWidth = 1;
                        ctxTrackRoute.strokeStyle = 'black';
                        ctxTrackRoute.stroke();
                        ctxTrackRoute.closePath();
                    }
                    pass++;
                } else {
                    pass = 0;
                }
            });
        }, 33);

        var xstart = 620;

        for (var a = 0; a < 11; a++) {
            if (a % 2) {
                ctxTrack.fillStyle = 'white';
            } else {
                ctxTrack.fillStyle = 'black';
            }

            ctxTrack.beginPath();
            ctxTrack.rect(xstart, 550, 7, 10);
            ctxTrack.fill();

            xstart += 7;
        }

        var xfinish = 715;

        for (var b = 0; b < 11; b++) {
            if (b % 2) {
                ctxTrack.fillStyle = 'white';
            } else {
                ctxTrack.fillStyle = 'black';
            }

            ctxTrack.beginPath();
            ctxTrack.rect(xfinish, 550, 7, 10);
            ctxTrack.fill();

            xfinish += 7;
        }

    }
</script>
</body>
</html>
