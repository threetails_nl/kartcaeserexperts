@extends('layouts.standard')

@section('content')

 {{--{{ dd($laptimes) }}--}}


 <div class="container">
     <table class="table">
         <thead>
         <tr>
             <th>Driver</th>
             <th>lap_1</th>
             <th>lap_2</th>
         </tr>
         </thead>
         <tbody>

         @foreach($laptimes as $laptime)
             <tr>
                 <td>{{ $laptime['naam'] }}</td>
                 <td>{{ $laptime['lap_1'] }}</td>
                 <td>{{ $laptime['lap_2'] }}</td>
             </tr>
         @endforeach

         </tbody>
     </table>


    <div class="row">
        <h1>Curren driving</h1>
    </div>
         <table class="table">
             <thead>
             <tr>
                 <th>Driver</th>
                 <th>Position</th>
                 <th>Time</th>
                 <th>Coördinates X/Y</th>
             </tr>
             </thead>
             <tbody>

             @foreach($kartlaps as $kartlap)
                 <tr>
                     <td>{{ $kartlap['kart_id'] }}</td>
                     <td>{{ $kartlap['kart_position'] }}</td>
                     <td>{{ $kartlap['kart_time'] }}</td>
                     <td>X: {{ $kartlap['kart_x'] }}<br/>Y: {{ $kartlap['kart_y'] }}</td>
                 </tr>
             @endforeach

             </tbody>
         </table>
 </div>
@endsection
