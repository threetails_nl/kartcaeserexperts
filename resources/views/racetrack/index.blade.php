<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            background-color: #000000;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        #racetrack {
            visibility: hidden;
        }

        #raceTrackRoute {
            position: absolute;
            top: 0;
            left: 0;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <img src="/img/Asset 1.png" id="racetrack">
        <canvas id="raceTrackRoute" width="1500" height="750">
            Your browser does not support the HTML5 canvas tag.
        </canvas>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script>
    window.onload = function () {
        var c = document.getElementById("raceTrackRoute");
        var ctx = c.getContext("2d");

        var img = document.getElementById("racetrack");
//        ctx.drawImage(img, 0, 0);
        var canvas = document.getElementById('raceTrackRoute');
        var context = canvas.getContext('2d');

        var color = ["red", "blue", "yellow", "purple", "green", "brown", "black"];

        setInterval(function () {
            $.get("/getracerdata", function (data) {
                console.log(data.length);
                for (var i = 0; i < data.length; i++) {
                    console.log(data);
                    console.log(data['kart_x']);

                    var y = (data[i]['kart_x'] + 40) * 10;
                    var x = (data[i]['kart_y'] + 70) * 10;

                    context.beginPath();
                    ctx.arc(x, y, 5, 0, 2 * Math.PI);

                    switch (data[i]['kart_id']) {
                        case "0xDECA31303540049E":
                            context.fillStyle = "red";
                            break;
                        case "0xDECA343036000F72":
                            context.fillStyle = "blue";
                            break;
                        case "0xDECA3130371013CA":
                            context.fillStyle = "yellow";
                            break;
                        case "0xDECA323035400489":
                            context.fillStyle = "purple";
                            break;
                        case "0xDECA3030354004C5":
                            context.fillStyle = "green";
                            break;
                        case "0xDECA3330354004B1":
                            context.fillStyle = "brown";
                            break;
                    }
                    context.fill();
//                    context
                    context.lineWidth = 1;
                    context.strokeStyle = 'white';
                    context.stroke();
                }
            });
        }, 250);

//        var xstart = 589;
//
//        for (var a = 0; a < 11; a++) {
//            if (a % 2) {
//                context.fillStyle = 'white';
//            } else {
//                context.fillStyle = 'black';
//            }
//
//            context.beginPath();
//            ctx.rect(xstart, 520, 6, 10);
//            context.fill();
//
//            xstart += 6;
//        }
//
//        var xfinish = 678;
//
//        for (var b = 0; b < 11; b++) {
//            if (b % 2) {
//                context.fillStyle = 'white';
//            } else {
//                context.fillStyle = 'black';
//            }
//
//            context.beginPath();
//            ctx.rect(xfinish, 520, 6, 10);
//            context.fill();
//
//            xfinish += 6;
//        }

    }
</script>
</body>
</html>
