<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            background-color: #c4c4c4;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        #racetrack {
            visibility: hidden;
        }

        #raceTrackRoute {
            position: absolute;
            top: 0;
            left: 0;
            background-color: rgba(255,255,255,0.5);
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h2 id="status">0 | 0</h2>
        <img src="/img/track.svg" id="racetrack">
        <canvas id="raceTrackRoute" width="1500" height="700"></canvas>

    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script>
    window.onload = function () {
        var ctx = document.getElementById('raceTrackRoute').getContext('2d');

        var img = document.getElementById("racetrack");
        ctx.drawImage(img, 0, 0);

        setInterval(function (event) {
        ctx.canvas.addEventListener('mousemove', function (event) {
            mouseX = event.clientX;
            mouseY = event.clientY;

                var status = document.getElementById('status');
                status.innerHTML = mouseX + " | " + mouseY;
        });
            console.log("array(\n\tarray(\"kart_id\" => \"1\", \"kart_position\" => \"0\", \"kart_speed\" => \"0.0\", \"kart_time\" => \"0.0\", \"kart_x\" => \"" + mouseX + "\", \"kart_y\" => \"" + mouseY + "\")\n),");
        }, 33);

        var xstart = 620;

        for (var a = 0; a < 11; a++) {
            if (a % 2) {
                ctx.fillStyle = 'white';
            } else {
                ctx.fillStyle = 'black';
            }

            ctx.beginPath();
            ctx.rect(xstart, 550, 7, 10);
            ctx.fill();

            xstart += 7;
        }

        var xfinish = 715;

        for (var b = 0; b < 11; b++) {
            if (b % 2) {
                ctx.fillStyle = 'white';
            } else {
                ctx.fillStyle = 'black';
            }

            ctx.beginPath();
            ctx.rect(xfinish, 550, 7, 10);
            ctx.fill();

            xfinish += 7;
        }

    };



</script>
</body>
</html>
